import lcm
import numpy as np
import matplotlib.pyplot as plt
import time
import math

import sys
sys.path.append('./lcmtypes')
from ctrlpanel import ctrlPanelData_t
from ctrlpanel import pidData_t

f1 = []
f2 = []
t = []

pid_in = []
pid_setpt = []
pid_out = []
pid_t = []

plt.ion()
fig0 = plt.figure(0)
plt.title('Control Panel')

fig1 = plt.figure(1)
plt.title('PID_Input')

fig2 = plt.figure(2)
plt.title('PID_Output')

def plot_handler(channel, data):
    msg = ctrlPanelData_t.decode(data)
    f1.append(msg.fader1)
    f2.append(msg.fader2)
    t.append(msg.timestamp)
    fig0.canvas.draw()

def pid_plot_handler(channel, data):
    msg = pidData_t.decode(data)
    pid_in.append(msg.pidInput)
    pid_out.append(msg.pidOutput)
    pid_t.append(msg.timestamp)
    fig1.canvas.draw()
    fig2.canvas.draw()

lc = lcm.LCM()
subscription = lc.subscribe("CTRL_PANEL_DATA", plot_handler)
subscription = lc.subscribe("PIDDATA", pid_plot_handler)

try:
    while True:
        lc.handle()
        plt.figure(0)
        plt.plot(t, f1, 'r')
        plt.plot(t, f2, 'b')
        plt.figure(1)
        plt.plot(pid_t,pid_in,'r')
        plt.figure(2)
        plt.plot(pid_t,pid_out,'b')

except KeyboardInterrupt:
    pass

lc.unsubscribe(subscription)
lc.unsubscribe(subscription_pid)

