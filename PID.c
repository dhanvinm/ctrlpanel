/*********************
*   PID.c
*   simple pid library
*   pgaskell
**********************/

#include "PID.h"
#include <stdio.h>
#include <stdlib.h>


#define MAX_OUTPUT 1.0
#define MIN_OUTPUT -1.0
#define ITERM_MIN -1.0
#define ITERM_MAX 1.0

#define FIRST_INPUT 999999

float clamp(float i,float min, float max)
{
    i = (i<min)? min:i;
    i = (i>max)? max:i;
    return i;
}
PID_t * PID_Init(float Kp, float Ki, float Kd) {
  // allocate memory for PID
  PID_t *pid =  malloc(sizeof(PID_t));

  //initalize values
  pid->pidInput = 0;
  pid->pidOutput = 0;
  pid->pidSetpoint = 0;
  pid->ITerm = 0;
  pid->prevInput = 0;

  pid->ki = Ki;
  pid->kd = Kd;
  pid->kp = Kp;
  // set update rate to 100000 us (0.1s)
  pid->updateRate = 100000;

  //set output limits, integral limits, tunings
  printf("initializing pid...\r\n");
  pid->ITerm = 0.0;
  pid->prevInput = FIRST_INPUT;

  //TODO
  pid->ITermMin = 0.0;
  pid->ITermMax = 100.0;
  pid->outputMin = 0.0;
  pid->outputMax = 100000;

  return pid;
}

void PID_Compute(PID_t* pid) {
  // Compute PID
    pid->pidOutput = 0.0;
    pid->pidOutput += pid->kp * pid->pidInput;
    pid->pidOutput += pid->kd * pid->pidInput - pid->prevInput;
    pid->prevInput = pid->pidInput;
    pid->ITerm += pid->pidInput;

    clamp(pid->ITerm,pid->ITermMin,pid->ITermMax);
    pid->pidOutput += pid->ki*pid->ITerm;
    clamp(pid->pidOutput,pid->outputMin,pid->outputMax);


}

void PID_SetTunings(PID_t* pid, float Kp, float Ki, float Kd) {
  //scale gains by update rate in seconds for proper units
  float updateRateInSec = ((float) pid->updateRate / 1000000.0);
  //set gains in PID struct
  pid->kp = Kp*updateRateInSec;
  pid->ki = Ki*updateRateInSec;
  pid->kd = Kd*updateRateInSec;
}

void PID_SetOutputLimits(PID_t* pid, float min, float max){
  //set output limits in PID struct
  pid->outputMax = max;
  pid->outputMin = min;
}

void PID_SetIntegralLimits(PID_t* pid, float min, float max){
  //set integral limits in PID struct
  pid->ITermMax = max;
  pid->ITermMin = min;

}

void PID_SetUpdateRate(PID_t* pid, int updateRate){
  //set integral limits in PID struct
    pid->updateRate = updateRate;
}





